import type { CookieParam } from "puppeteer-core";
import { Config } from ".";

export const CP = [
    {
        name: "__cf_bm",
        value: "gfq3MRgaX_4XwXkhPZrbTCWUdutN2kApRwW3QGAJKTc-1719348023-1.0.1.1-OgQHXN6LEyPsFVJT4EJRC2fALlwVcDUVqZlZ2dqe7pLQaJ.8HHQckpL9aIeCK72WDKxtLy_wLNxj895E0RBn6w",
        path: "/",
        domain: ".js.ubembed.com",
    },
    {
        name: Config.SESSION_COOKIE_NAME,
        value: Config.SESSION_COOKIE,
        path: "/",
        domain: ".www.matchtech.com",
        secure: false,
        httpOnly: true,
        sameSite: "Strict",
    },
] as CookieParam[]