import { getQuote } from "./quotes";

const getEnv = () => {
  let { SESSION_COOKIE,SESSION_COOKIE_NAME, CF, PDF_PATH, DEBUG } = process.env;


  if (SESSION_COOKIE === undefined) throw new Error("SESSION_COOKIE is undefined");
  if(SESSION_COOKIE_NAME === undefined) throw new Error("SESSION_COOKIE_NAME is undefined");
  if (CF === undefined) throw new Error("CF is undefined");
  if (PDF_PATH === undefined) throw new Error("PDF_PATH is undefined");


  return {
    SESSION_COOKIE,
    PDF_PATH,
    SESSION_COOKIE_NAME,
    DEBUG: DEBUG?.toLowerCase() === "true" ?? false,
  };
};
export const debugLog = (msg) => {
  if (Config.DEBUG) console.log(msg);
};


export const Config = getEnv();

const main = async () => {
  let r = await getQuote(1, 1);
  console.log(r);
};

main();
