import puppeteer from "puppeteer";
import { Config, debugLog } from ".";
import { CP } from "./cookieParams";
const getUrl = (page = 0) => {
  return (
    `https://www.matchtech.com/search-page?title=&location_autocomplete=&location%5Bdistance%5D%5Bfrom%5D=30&location%5Bvalue%5D=&sort_by=relevance` +
    `&page=${page}`
  );
};
export const getQuote = async (startFromPage = 0, pageNumber = 1) => {

  const browser = await puppeteer.launch({
    headless: false,
    executablePath: process.env.CHROME_PATH,
    defaultViewport: null,
  });

  let page = await browser.newPage();
  page.setCookie(
    ...CP
  );

  console.log(startFromPage);
  let i = startFromPage
  for (; i < pageNumber + startFromPage; i++) {
    await page.goto(getUrl(i), {
      waitUntil: "domcontentloaded",
    });

    let nr = await page.evaluate(async () => {
      const container = document.querySelector(".job-listings");
      const elements = container.querySelectorAll("a.job--search");

      let r = [];
      for (const element of elements) {
        r.push(element.getAttribute("href"));
      }
      return r;
    });

    let i = 0;
    for (let p in nr) {
      console.log(i++);
      page.locator(`a[href="${nr[p]}"]`).click();
      await new Promise((resolve) => setTimeout(resolve, 1000));

      let isthereApply = await page.evaluate(async () => {
        const container = document.querySelector(
          "button.apply-now.btn.btn-brand.uppercase"
        );
        if (container == null) {
          return false;
        }
        return true;
      });
      if (!isthereApply) {
        debugLog("no apply");
        continue;
      }
      await new Promise((resolve) => setTimeout(resolve, 1000));

      let app = page.locator("button.apply-now.btn.btn-brand.uppercase");
      app.click();

      await new Promise((resolve) => setTimeout(resolve, 2000));
      // continue

      let isthereFileInput = await page.evaluate(async () => {
        const container = document.querySelector('input[type="file"]');
        if (container == null) {
          return false;
        }
        return true;
      });
      if (!isthereFileInput) {
        console.log("no file input1");
        continue;
      }

      await page.waitForSelector("input[type=file]");
      const fileInput = await page.$("input[type=file]");
      await fileInput.uploadFile(Config.PDF_PATH);
      await fileInput.evaluate((upload) =>
        upload.dispatchEvent(new Event("change", { bubbles: true }))
      );
      await new Promise((resolve) => setTimeout(resolve, 1000));

      await page.waitForSelector('input[type="submit"]');
      await page.locator('input[type="submit"]').click();
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
  }

  await page.close();
  await browser.close();
};
