{ pkgs ? import <nixpkgs> { } }: pkgs.mkShell {
  packages = with pkgs; [
    #chromium
  ];
  shellHook = ''
    export PUPPETEER_EXECUTABLE_PATH=$(which google-chrome-stable)
    #export PUPPETEER_EXECUTABLE_PATH=$(which chromium)
    export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
    echo shell
  '';
}